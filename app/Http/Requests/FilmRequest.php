<?php
/*
 * Created on Sat Nov 30 2019
 *
 * Copyright (c) 2019 Kpono-Abasi Akpabio
 */
namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;


class FilmRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'unique:films',
            'description'=> 'required',
            'release_date' => 'required',
            'rating'=> 'required',
            'ticket_price'=>'required',
            'country'=>'required',
            'genres'=>'required',
            'photo'=>'required'
        ];
    }
}
