<?php
/*
 * Created on Sat Nov 30 2019
 *
 * Copyright (c) 2019 Kpono-Abasi Akpabio
 */
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\Film;
use App\Http\Resources\FilmResource;
use App\Http\Requests\FilmRequest;
use Symfony\Component\HttpFoundation\Response;


class FilmController extends Controller
{


    public function __construct()
    {
        $this->middleware('jwt', ['except' => ['index','show']]);
    }

    /**
     * Fetch all films.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
     
        return FilmResource::collection(Film::latest()->get());
    }

    
    /**
     * Fetch a single film.
     *
     * @return \Illuminate\Http\Response
     */
    public function show(Film $film)
    {
        return new FilmResource($film);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(FilmRequest $request)
    {
        $film=Film::create($request->all());
		
        return response(new FilmResource($film), Response::HTTP_CREATED);


    }


}
