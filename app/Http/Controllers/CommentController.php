<?php
/*
 * Created on Sat Nov 30 2019
 *
 * Copyright (c) 2019 Kpono-Abasi Akpabio
 */
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\Comment;
use App\Model\Film;
use App\Http\Resources\CommentResource;
use App\Http\Requests\CommentRequest;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Support\Facades\DB;


class CommentController extends Controller
{
     /**
     * Create a new AuthController instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('JWT', ['except' =>['index','show'] ]);
    }

    /**
     * Display a listing of comments based on film.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Film $film)
    {
        return CommentResource::collection($film->comments);
       
    }
    
    

    /**
     * Store a newly created comment in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CommentRequest $request)
    {
        $user_id=auth()->user()->id;
       

        $id=DB::table('comments')->insertGetId(
            [
                'user_id'=>4,
                'film_id'=>$request->film_id,
                'comment'=>$request->comment,
                'created_at'=>date('Y-m-d')
            ]
        );
        $comment=Comment::where('id',$id)->first();

    
        return response(new CommentResource($comment), Response::HTTP_CREATED);


    }


}
