<?php
/*
 * Created on Sat Nov 30 2019
 *
 * Copyright (c) 2019 Kpono-Abasi Akpabio
 */
namespace App\Http\Resources;

use App\User;
use Illuminate\Http\Resources\Json\JsonResource;

class CommentResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {

        return [
            'id' => $this->id,
            'comment' => $this->comment,
            'created_at'=>$this->created_at->diffForHumans(),
            'user' => $this->user->where('id', $this->user_id)->first()->name     
        ];
    }
}
