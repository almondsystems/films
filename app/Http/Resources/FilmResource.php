<?php
/*
 * Created on Sat Nov 30 2019
 *
 * Copyright (c) 2019 Kpono-Abasi Akpabio
 */
namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Model\FilmGenres;
use Illuminate\Support\Facades\DB;

class FilmResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $genreRecords = DB::select('SELECT name from genres where id IN (SELECT genre_id from film_genres WHERE film_id='.$this->id.')');
 
        $genres="";
        foreach($genreRecords as $genre)
            $genres=$genre->name.",";
            
        $genres=rtrim($genres,',');

        return [
            'id'=>$this->id,
            'name'=>$this->name,
            'description'=>$this->description,
            'release_date'=>$this->release_date,
            'rating'=>$this->rating,
            'ticket_price'=>$this->ticket_price,
            'country'=>$this->country,
            'genres'=>$genres,
            'comments' => CommentResource::collection($this->comments),
            'photo'=>$this->photo,
            'slug'=>$this->slug,
            'path'=>'/films/'.$this->slug
        ];
    }
}
