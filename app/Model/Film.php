<?php
/*
 * Created on Sat Nov 30 2019
 *
 * Copyright (c) 2019 Kpono-Abasi Akpabio
 */

namespace App\Model;
use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Model;


class Film extends Model
{
    protected $fillable=['name','description','release_date','rating', 'ticket_price', 'country','genre_id','photo','slug'];
    protected $with = ['comments'];

    protected static function boot()
    {
        parent::boot();
        static::creating(function ($film) {
            $film->slug = Str::slug($film->name);
        });           
        static::updating(function ($film) {
            $film->slug =Str::slug($film->name);
        });    
      
    }
     //generate slug
    public function getRouteKeyName()
    {
         return 'slug';
    }
 

   public function comments()
   {
       return $this->hasMany(Comment::class)->latest();
   }

   public function genres()
   {
    return $this->belongsToMany(Genre::class);
    
   }

   public function getPathAttribute()
    {
      
          return "/films/$this->slug";

    }
}
