<?php
/*
 * Created on Sat Nov 30 2019
 *
 * Copyright (c) 2019 Kpono-Abasi Akpabio
 */

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Genre extends Model
{
    protected $fillable=['name'];

public function films()
{
   return $this->belongsToMany(Film::class);
   
}



}
