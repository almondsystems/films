<?php
/*
 * Created on Sat Nov 30 2019
 *
 * Copyright (c) 2019 Kpono-Abasi Akpabio
 */

namespace App\Model;
use App\User;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    protected $fillable=['comment','user_id','film_id'];
    protected static function boot()
    {
        parent::boot();

        static::creating(function ($comment) {
            //$comment->user_id = auth()->id(); pending auth
            $comment->user_id = rand(1,2);
        });
    }

    public function film()
    {
        return $this->belongsTo(Film::class);
    }

    
    public function user()
   {
       return $this->belongsTo(User::class, 'user_id');
   }
    
   
}
