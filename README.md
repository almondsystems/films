1. Ensure you have installed Xammp or Wamp or any PHP/Mysql server with Php 7.0 and above
2. Ensure Mysql is running and you have created a database called film
3.Clone the repository- https://bitbucket.org/almondsystems/films
4. Ensure you have installed Composer and NodeJS
5. Enter the root folder and run "composer install"
6. Enter the root folder and run "npm install"
7. Enter the root folder and run "php artisan serve"
8.Enter the root folder and run "npm run watch"
9. Access the app via http://localhost:8000/films/ on browser and http://localhost:8000/api/films/  on Postman
10.  http://localhost:8000/create for creating films
