
/*
 * Created on Mon   Dec 02 2019
 *
 * Copyright (c) 2019 Kpono-Abasi Akpabio
 */
import User from './User';

class Exception{
    handle(error){
        this.isExipred(error.response.data.error)
    }

    isExipred(error){
        if(error == 'Token is invalid'){
            User.logout()
        }
    }
}

export default Exception = new Exception()