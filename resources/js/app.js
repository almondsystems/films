/*
 * Created on Sun Dec 01 2019
 *
 * Copyright (c) 2019 Kpono-Abasi Akpabio
 */
require('./passToken');

require('./bootstrap');

window.Vue = require("vue");
import Vue from "vue";
import Vuetify from "vuetify";
const vuetifyOptions = { }
window.axios = require("axios");

import User from './Helpers/User'
window.User = User

import Exception from './Helpers/Exception'
window.Exception = Exception

window.EventBus = new Vue();


Vue.use(Vuetify);


Vue.component("AppHome", require("./components/AppHome.vue").default);
import router from "./Router/router.js";

const app = new Vue({
    el: "#app",
    router,
    vuetify: new Vuetify(vuetifyOptions)
});
