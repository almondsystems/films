/*
 * Created on Sun Dec 01 2019
 *
 * Copyright (c) 2019 Kpono-Abasi Akpabio
 */

import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

import Login from '../components/login/Login'
import Films from '../components/Films'
import Single from '../components/Single'
import Create from '../components/Create'
import Signup from '../components/login/Signup'
import Logout from '../components/login/Logout'


const routes = [
    {
        path: '/films/login',
        component: Login,
        name:'name'
    },
    {
        path: '/films/signup',
        component: Signup
    },
    {
        path: '/films/logout',
        component: Logout
    },
    {
        path: '/films',
        component: Films,
        name: 'films'
    },
    {
        path: '/films/:slug',
        component: Single,
        name: 'single'
    },
    {
        path: '/create',
        component: Create
    }
]
const router = new VueRouter({
    routes, // short for `routes: routes`,
    hashbang: false,
    mode: 'history'
})


export default router