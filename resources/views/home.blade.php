<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="stylesheet" href="{{ asset('css/app.css') }} ">

    <title>Films</title>


    
    

    @yield('css')
</head>
<body id="app-layout">

<div id="app">
        <v-app>
            <app-home></app-home>
        </v-app>
    </div>

<footer class="footer">
    <div class="container">
        Copyright &copy; Films {{ date('Y') }} - All rights reserved
        
    </div>
</footer>






@yield('scripts')
<script src="{{asset('js/app.js')}}"></script>
</body>
</html>
