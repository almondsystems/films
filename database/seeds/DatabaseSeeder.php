<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Model\Film;
use App\Model\Comment;
use Faker\Generator as Faker;



class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run(Faker $faker)
    {
        factory(User::class, 3)->create();

        //seed films
        $nameJasmine='Jasmine';
        DB::table('films')->insert([
            'name' =>$nameJasmine,
            'description' => 'He is the dream husband, she is the spoilt wife, their marriage is perfect . All of a sudden, she wants more, he wants more. Can this marriage be saved?',
            'slug'=>Str::slug($nameJasmine,'-'),
            'release_date' => $faker->dateTimeBetween($startDate = '-1 years', $endDate = 'now'),
            'rating'=> $faker->numberBetween(1,5),
            'ticket_price'=>$faker->numberBetween(10000,100000),
            'country'=>'Nigeria',
            'created_at'=>date('Y-m-d'),
            'photo'=>'/images/jasmine.png'
            ]);


            $nameSeven='Seven';
            DB::table('films')->insert([
                'name' =>$nameSeven,
                'description' => 'Mr Tayo, a wealthy man from humble beginnings, discovers he has an incurable brain tumour which will cuts his life short within the short time he has left, he hands over his empire to his immature rebellious only son Kolade. Tayo develops a plan to send his son to Ajegunle, same place Tayo grew up for a week with no money and no help from the outside world with the hope that Kolade will grow from the adversity and become a worthy heir.',
                'slug'=>Str::slug($nameSeven,'-'),
                'release_date' => $faker->dateTimeBetween($startDate = '-1 years', $endDate = 'now'),
                'rating'=> $faker->numberBetween(1,5),
                'ticket_price'=>$faker->numberBetween(10000,100000),
                'country'=>'Nigeria',
                'created_at'=>date('Y-m-d'),
                'photo'=>'/images/seven.png'
                ]);

                $namePf='Playing with Fire';
                DB::table('films')->insert([
                    'name' =>$namePf,
                    'description' => 'A crew of rugged firefighters meet their match when attempting to rescue three rambunctious kids.',
                    'slug'=>Str::slug($namePf,'-'),
                    'release_date' => $faker->dateTimeBetween($startDate = '-1 years', $endDate = 'now'),
                    'rating'=> $faker->numberBetween(1,5),
                    'ticket_price'=>$faker->numberBetween(10000,100000),
                    'country'=>'Nigeria',
                    'created_at'=>date('Y-m-d'),
                    'photo'=>'/images/playing_with_fire.png'
                    ]);


                 //seed comments
                 
                 DB::table('comments')->insert([
                    'comment' => 'Great movie',
                    'user_id'=> 1,
                    'film_id' => 1,
                    'created_at'=>date('Y-m-d')
                ]);

                DB::table('comments')->insert([
                    'comment' => 'Very funny movie',
                    'user_id'=> 2,
                    'film_id' => 2,
                    'created_at'=>date('Y-m-d')
                ]);


                DB::table('comments')->insert([
                    'comment' => 'Boring',
                    'user_id'=> 1,
                    'film_id' => 3,
                    'created_at'=>date('Y-m-d')
                ]);

            //seed genres
            DB::table('genres')->insert([
                'name' => 'Drama'
            ]);

            DB::table('genres')->insert([
                'name' => 'Comedy'
            ]);

            DB::table('genres')->insert([
                'name' => 'Action'
            ]);

            //seed genre assignment to films
            DB::table('film_genres')->insert([
                'film_id' => 1,
                'genre_id'=>1
            ]);

            DB::table('film_genres')->insert([
                'film_id' => 2,
                'genre_id'=>2
            ]);

            DB::table('film_genres')->insert([
                'film_id' => 3,
                'genre_id'=>3
            ]);
    }
}
